package cz.pef.coinwhale.ui.extensions

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController

//zmeni z previous na current
fun <T> NavController.getValue(key: String): MutableLiveData<T>? {
    return this.currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)
}


fun <T> NavController.removeValue(key: String) {
    this.currentBackStackEntry
        ?.savedStateHandle
        ?.remove<T>(key)


}

