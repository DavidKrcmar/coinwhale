package cz.pef.coinwhale.ui.screens


import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.pef.coinwhale.R
import cz.pef.coinwhale.navigation.INavigationRouter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddEditAccountScreen(navigationRouter: INavigationRouter) {

    val viewModel = hiltViewModel<AddEditAccountViewModel>()
    var data by remember {
        mutableStateOf(AddEditScreenData())
    }

    val state = viewModel.addEditAccountUIState.collectAsState()

    state.value.let {
        when(it){
            AddEditAccountUIState.AccountSaved -> {
                LaunchedEffect(it){
                    navigationRouter.returnBack()
                }
            }
            is AddEditAccountUIState.AccountStateChange -> {
                data = it.data
            }
            AddEditAccountUIState.Loading -> {
            }
        }
    }


    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                title = {
                    Text(text = stringResource(id = R.string.add_account))
                },
            )},

        ) {
        AddEditAccountScreenContent(
            paddingValues = it,
            actions = viewModel,
            data = data

        )
    }
}


@Composable
fun AddEditAccountScreenContent(
    paddingValues: PaddingValues,
    actions: AddEditAccountScreenActions,
    data: AddEditScreenData
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .align(Alignment.Center)
        ) {
            OutlinedTextField(
                label = { Text(stringResource(id = R.string.name)) },
                value = data.account.name,
                onValueChange = { actions.onTextChange(it) },
                isError = data.taskNameError != null,
                supportingText = {
                    if (data.taskNameError != null)
                        Text(text = data.taskNameError!!)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 16.dp, end = 16.dp)
            )
        }

        Button(
            onClick = { actions.saveAccount() },
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
                .height(56.dp)
        ) {
            Text(text = stringResource(id = R.string.save))
        }
    }
}

