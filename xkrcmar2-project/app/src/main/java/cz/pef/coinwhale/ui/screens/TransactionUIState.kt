package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.model.Transaction
import cz.pef.coinwhale.model.TransactionWithCategory


sealed class TransactionUIState {
    class Loading : TransactionUIState()
    class Success(val transactions: List<TransactionWithCategory>) : TransactionUIState()


}
