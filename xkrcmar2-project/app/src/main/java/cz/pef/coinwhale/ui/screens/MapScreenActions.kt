package cz.pef.coinwhale.ui.screens

interface MapScreenActions {
    fun onLocationChange(latitude: Double,longitude: Double)

}
