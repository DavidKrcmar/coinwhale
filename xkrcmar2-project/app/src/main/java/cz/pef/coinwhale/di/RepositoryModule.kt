package cz.pef.coinwhale.di

import cz.pef.coinwhale.database.Dao
import cz.pef.coinwhale.database.ILocalRepository
import cz.pef.coinwhale.database.LocalRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(dao: Dao): ILocalRepository {
        return LocalRepositoryImpl(dao)
    }

}
