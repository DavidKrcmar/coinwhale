package cz.pef.coinwhale.ui.screens


sealed class AddEditAccountUIState {

    object Loading : AddEditAccountUIState()
    object AccountSaved : AddEditAccountUIState()

    class AccountStateChange(val data: AddEditScreenData) : AddEditAccountUIState()




}
