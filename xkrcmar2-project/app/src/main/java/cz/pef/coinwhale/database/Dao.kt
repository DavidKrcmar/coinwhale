package cz.pef.coinwhale.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction
import cz.pef.coinwhale.model.TransactionWithCategory
import kotlinx.coroutines.flow.Flow

@Dao
interface Dao {
    //Account
    @Query("SELECT * FROM account")
    fun getAllAccounts(): Flow<List<Account>>
    @Insert
    suspend fun insertAccount(account: Account): Long

    @Update
    suspend fun updateAccount(account: Account)

    @Delete
    suspend fun deleteAccount(account: Account)

    //Category
    @Query("SELECT * FROM category")
    fun getAllCategories(): Flow<List<Category>>

    @Insert
    suspend fun insertCategory(category: Category): Long

    @Update
    suspend fun updateCategory(category: Category)

    @Delete
    suspend fun deleteCategory(category: Category)

    @Query("SELECT * FROM category WHERE id = :id")
    suspend fun getCategory(id: Long): Category

    @Query("SELECT * FROM category WHERE isIncome = :isIncome")
    fun getOneTypeOfCategories(isIncome: Boolean): List<Category>


    //Transaction
    @Insert
    suspend fun insertTransaction(transaction: Transaction): Long

    @Query("SELECT * FROM `transaction` JOIN Category ON `transaction`.categoryId = Category.id WHERE `transaction`.accountId = :accountId")
    fun getAccountTransactions(accountId: Long): Flow<List<TransactionWithCategory>>

    @Update
    suspend fun updateTransaction(transaction: Transaction)

    @Query("SELECT * FROM `transaction` WHERE transactionId = :transactionId")
    suspend fun getTransactionDetail(transactionId: Long): Transaction


    @Delete
    suspend fun deleteTransaction(transaction: Transaction)

}
