package cz.pef.coinwhale.ui.screens


import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.pef.coinwhale.database.ILocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class StatisticsViewModel @Inject constructor(private val repository: ILocalRepository) : ViewModel() {

    val statisticsListUIState: MutableState<StatisticsListUIState> = mutableStateOf(StatisticsListUIState.Loading())

    fun loadCategories(id: Long){
        viewModelScope.launch {
            repository.getAccountTransactions(id).collect {
                statisticsListUIState.value = StatisticsListUIState.Success(it)
            }
        }
    }


}
