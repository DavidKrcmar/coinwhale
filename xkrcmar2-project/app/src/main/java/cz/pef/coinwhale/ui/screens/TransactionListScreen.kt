package cz.pef.coinwhale.ui.screens


import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.pef.coinwhale.R
import cz.pef.coinwhale.datastore.StoreCurrencyProvider
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction
import cz.pef.coinwhale.model.TransactionWithCategory
import cz.pef.coinwhale.navigation.INavigationRouter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TransactionListScreen(navigationRouter: INavigationRouter,id: Long) {

    val viewModel = hiltViewModel<TransactionListViewModel>()

    val transactions: MutableList<TransactionWithCategory> = mutableListOf()


    viewModel.transactionListUIState.value.let {
        when(it){
            is TransactionUIState.Loading -> {
                viewModel.loadTransactions(id)
            }
            is TransactionUIState.Success -> {
                transactions.addAll(it.transactions)
            }
        }
    }



    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                title = {
                    Text(text = "Account")  //TODO change for real name of the account
                },
                actions = {
                    if (transactions.size > 0){
                        IconButton(onClick = { navigationRouter.navigateToStatistics(id=id)
                             }) {
                        Image(painter = painterResource(id = R.drawable.pie_chart), contentDescription = null)
                        }
                    }
                }
            )},
        floatingActionButton = {
            FloatingActionButton(
                onClick = { navigationRouter.navigateToAddTransaction(id, null) },
                content = {
                    Row(
                        modifier = Modifier.padding(8.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Icon(imageVector = Icons.Filled.Add, contentDescription = null)
                        Text(stringResource(id = R.string.add_transaction))
                    }
                }
            )
        }

    ) {
        TransactionListScreenContent(paddingValues = it, navigationRouter = navigationRouter, transactions = transactions, id = id)
    }
}

@Composable
fun TransactionListScreenContent(
    paddingValues: PaddingValues,
    navigationRouter: INavigationRouter,
    transactions: List<TransactionWithCategory>,
    id: Long
) {
    val savedCurrency = StoreCurrencyProvider.getInstance().getCurrency.collectAsState(initial = "")
    Column(modifier = Modifier.padding(paddingValues)) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .height(150.dp)
                .background(Color.Gray, shape = RoundedCornerShape(16.dp))
                .padding(start = 32.dp, end = 32.dp),
            verticalArrangement = Arrangement.Center,
        ) {
            Text(stringResource(id = R.string.balance) + getSum(transactions) + savedCurrency.value, fontSize = 30.sp)
            Spacer(modifier = Modifier.height(20.dp))
            Column {
                Text(stringResource(id = R.string.expenses) + getExpensesSum(transactions) + savedCurrency.value,fontSize = 20.sp,modifier = Modifier.padding(end = 8.dp))
                Text(stringResource(id = R.string.incomes) + getIncomesSum(transactions) + savedCurrency.value,fontSize = 20.sp, modifier = Modifier.padding(end = 8.dp))
            }

        }

    LazyColumn{
        transactions.forEach {
            item {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            navigationRouter.navigateToAddTransaction(id, it.transaction.transactionId)
                        }
                        .padding(16.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Text(
                        modifier = Modifier
                            .padding(16.dp)
                            .drawBehind {
                                drawCircle(
                                    color = Color(it.category.color!!),
                                    radius = this.size.maxDimension
                                )
                            },
                        text = it.category.name[0].toString().uppercase(),
                        color = Color.Black,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold
                    )

                    Spacer(modifier = Modifier.width(16.dp))
                    Column {
                        Text(text = it.transaction.transactionName,fontWeight = FontWeight.Bold,fontSize = 20.sp)

                        if(it.transaction.description == null){
                            Text(text = stringResource(R.string.no_description),fontSize = 16.sp, color = Color.Gray)
                        }else if (it.transaction.description!!.length > 20){
                            Text(text = it.transaction.description!!.take(20) + "...",fontSize = 16.sp, color = Color.Gray)
                        }else{
                            Text(text = it.transaction.description!!,fontSize = 16.sp, color = Color.Gray)
                        }
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    Text(text = it.transaction.price.toString() + savedCurrency.value, fontSize = 20.sp, modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(end = 8.dp))
                    
                }
            }
        }
    }


    }

}


@Composable
fun getSum(data:  List<TransactionWithCategory>): Double{
    return getIncomesSum(data) - getExpensesSum(data)
}

@Composable
fun getExpensesSum(data:  List<TransactionWithCategory>): Double{
    var sum: Double = 0.0
    for (transaction in data) {
        if(!transaction.category.isIncome){
            sum += transaction.transaction.price
        }
    }
    return sum
}

@Composable
fun getIncomesSum(data:  List<TransactionWithCategory>): Double{
    var sum: Double = 0.0
    for (transaction in data) {
        if(transaction.category.isIncome){
            sum += transaction.transaction.price
        }
    }
    return sum
}