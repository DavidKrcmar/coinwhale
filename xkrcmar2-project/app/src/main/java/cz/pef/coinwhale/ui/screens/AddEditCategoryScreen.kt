package cz.pef.coinwhale.ui.screens


import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.pef.coinwhale.R
import cz.pef.coinwhale.navigation.INavigationRouter
import cz.pef.coinwhale.ui.elements.ColorPicker
import cz.pef.coinwhale.ui.elements.TwoStateToggle


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddEditCategoryScreen(navigationRouter: INavigationRouter,id: Long?) {

    val viewModel = hiltViewModel<AddEditCategoryViewModel>()
    var data by remember {
        mutableStateOf(AddEditCategoryData())
    }

    val state = viewModel.addEditCategoryUIState.collectAsState()

    state.value.let {
        when(it){
            AddEditCategoryUIState.CategoryDeleted -> {
                LaunchedEffect(it){
                    navigationRouter.returnBack()
                }
            }
            AddEditCategoryUIState.CategorySaved -> {
                LaunchedEffect(it){
                    navigationRouter.returnBack()
                }

            }
            is AddEditCategoryUIState.CategoryStateChange -> {
                data = it.data
            }
            AddEditCategoryUIState.Loading -> {
                viewModel.loadCategory(id)
            }
        }
    }


    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                actions = {
                    if(id != null){
                        IconButton(onClick = { viewModel.deleteCategory() }) {
                            Icon(imageVector = Icons.Default.Delete, contentDescription = null)
                        }}
                },
                title = {
                    Text(text = stringResource(id = R.string.add_category))
                },

            )},


        ) {
        AddEditCategoryScreenContent(
            paddingValues = it,
            actions = viewModel,
            data = data

        )
    }
}


@Composable
fun AddEditCategoryScreenContent(
    paddingValues: PaddingValues,
    actions: AddEditCategoryScreenActions,
    data: AddEditCategoryData
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .align(Alignment.Center)
        ) {

            TwoStateToggle(actions = actions,data = data)

            OutlinedTextField(
                label = { Text(stringResource(id = R.string.name)) },
                value = data.category.name,
                onValueChange = { actions.onTextChange(it) },
                isError = data.NameError != null,
                supportingText = {
                    if (data.NameError != null)
                        Text(text = data.NameError!!)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 16.dp, end = 16.dp)
            )

            Text(text = stringResource(id = R.string.color), modifier = Modifier.padding(start = 16.dp, bottom = 8.dp))
            ColorPicker(actions = actions, data = data)
        }


        Button(
            onClick = { actions.saveCategory() },
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
                .height(56.dp)
        ) {
            Text(text = stringResource(id = R.string.save))
        }
    }
}
