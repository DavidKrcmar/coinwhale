package cz.pef.coinwhale.di


import cz.pef.coinwhale.database.CoinWhaleDatabase
import cz.pef.coinwhale.database.Dao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DaoModule {

    @Provides
    @Singleton
    fun provideDao(database: CoinWhaleDatabase): Dao {
        return database.Dao()
    }

}
