package cz.pef.coinwhale.ui.screens


import cz.pef.coinwhale.model.Category


sealed class CategoryListUIState {
    class Loading : CategoryListUIState()
    class Success(val categories: List<Category>) : CategoryListUIState()




}