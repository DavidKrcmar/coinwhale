package cz.pef.coinwhale

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.platform.LocalContext
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import cz.pef.coinwhale.datastore.StoreCurrency
import cz.pef.coinwhale.datastore.StoreCurrencyProvider
import cz.pef.coinwhale.navigation.Destination
import cz.pef.coinwhale.navigation.NavGraph
import cz.pef.coinwhale.ui.theme.CoinWhaleTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StoreCurrencyProvider.initialize(applicationContext)
        //clearCurrencyData()
        installSplashScreen()

        setContent {
            CoinWhaleTheme {
                NavGraph(startDestination = Destination.MainPageScreen.route)
            }
        }
    }
}




private fun clearCurrencyData() {
    CoroutineScope(Dispatchers.IO).launch {
        try {
            StoreCurrencyProvider.getInstance().clearCurrencyData()
        } catch (e: Exception) {
            Log.e("ClearCurrencyData", "Error clearing currency data: ${e.message}")
        }
    }
}
