package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.Category

class AddEditCategoryData {
    var category: Category = Category("")
    var NameError: String? = null
}
