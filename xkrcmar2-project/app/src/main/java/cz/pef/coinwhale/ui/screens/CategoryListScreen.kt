package cz.pef.coinwhale.ui.screens


import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.pef.coinwhale.R
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.navigation.INavigationRouter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryListScreen(navigationRouter: INavigationRouter) {
    val viewModel = hiltViewModel<CategoryListViewModel>()

    val categories: MutableList<Category> = mutableListOf()


    viewModel.categoryListUIState.value.let {
        when(it){
            is CategoryListUIState.Loading -> {
                viewModel.loadCategories()
            }
            is CategoryListUIState.Success -> {
                categories.addAll(it.categories)
            }
        }
    }


    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                title = {
                    Text(text = stringResource(id = R.string.categories))
                },
            )},
            floatingActionButton = {
                FloatingActionButton(
                    onClick = { navigationRouter.navigateToAddEditCategory(null) },
                    content = {
                        Row(
                            modifier = Modifier.padding(8.dp),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Icon(imageVector = Icons.Filled.Add, contentDescription = null)
                            Text(text = stringResource(id = R.string.add_category))
                        }
                    }
                )
            }
    ) {
        CategoryListScreenContent(paddingValues = it, categories = categories,navigationRouter = navigationRouter)
    }
}

@Composable
fun CategoryListScreenContent(
    paddingValues: PaddingValues,
    categories: List<Category>,
    navigationRouter: INavigationRouter

) {
    LazyColumn(
        modifier = Modifier.padding(paddingValues)
    ) {
        categories.forEach {
            item {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            navigationRouter.navigateToAddEditCategory(it.id)
                        }
                        .padding(8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    CircleWithLetter(letter = it.name[0], color = it.color!!)
                    Spacer(modifier = Modifier.width(16.dp))
                    Column {
                        Text(text = it.name,fontWeight = FontWeight.Bold,fontSize = 16.sp)
                        Text(text = if(it.isIncome){"Income"}else{"Expense"}, color = Color.Gray, fontSize = 12.sp)
                    }
                }
            }
        }
    }


}

@Composable
fun CircleWithLetter(letter: Char, color: Int) {

    Text(
        modifier = Modifier
            .padding(16.dp)
            .drawBehind {
                drawCircle(
                    color = Color(color),
                    radius = this.size.maxDimension
                )
            },
        text = letter.toString().uppercase(),
        color = Color.Black,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold
    )
}
