package cz.pef.coinwhale.ui.screens

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.pef.coinwhale.database.ILocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class CategoryListViewModel @Inject constructor(private val repository: ILocalRepository) : ViewModel(),CategoryListScreenActions {

    val categoryListUIState: MutableState<CategoryListUIState> = mutableStateOf(CategoryListUIState.Loading())

    fun loadCategories(){
        viewModelScope.launch {
            repository.getAllCategories().collect {
                categoryListUIState.value = CategoryListUIState.Success(it)
            }
        }
    }


}
