package cz.pef.coinwhale.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "account")
data class Account(var name: String){

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null

}
