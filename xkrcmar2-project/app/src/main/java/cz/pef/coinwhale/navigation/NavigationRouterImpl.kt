package cz.pef.coinwhale.navigation

import androidx.navigation.NavController
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import cz.pef.coinwhale.model.Location

class NavigationRouterImpl(private val navController: NavController) : INavigationRouter {
    override fun navigateToCategoryList() {
        navController.navigate(Destination.CategoryListScreen.route)
    }

    override fun navigateToAccountList() {
        navController.navigate(Destination.AccountListScreen.route)
    }

    override fun navigateToTransactionList(id: Long) {
        navController.navigate(Destination.TransactionListScreen.route + "/" + id)
    }

    override fun navigateToAddEditAccount(id: Long?) {
        navController.navigate(Destination.AddEditAccountScreen.route)
    }

    override fun navigateToAddEditCategory(id: Long?) {
        if(id == null){
            navController.navigate(Destination.AddEditCategoryScreen.route)
        }else {
            navController.navigate(Destination.AddEditCategoryScreen.route + "/" + id)
        }
    }

    override fun navigateToStatistics(id: Long) {
        navController.navigate(Destination.StatisticsScreen.route + "/" + id)
    }

    override fun navigateToAddTransaction(id: Long, transactionId: Long?) {
        if(transactionId == null){
            navController.navigate(Destination.AddTransactionScreen.route + "/" + id)
        }else {
            navController.navigate(Destination.AddTransactionScreen.route + "/" + id + "/" + transactionId)
        }
    }

    override fun navigateToMap(latitude: Double?, longitude: Double?) {
        if (latitude != null && longitude != null) {
            val moshi: Moshi = Moshi.Builder().build()
            val jsonAdapter: JsonAdapter<Location> = moshi.adapter(Location::class.java)
            navController.navigate(Destination.MapScreen.route + "/" + jsonAdapter.toJson(Location(latitude, longitude)))
        } else {
            navController.navigate(Destination.MapScreen.route)
        }
    }


    override fun returnFromMap(latitude: Double, longitude: Double) {
        val moshi: Moshi = Moshi.Builder().build()
        val jsonAdapter: JsonAdapter<Location> = moshi.adapter(Location::class.java)


        navController.previousBackStackEntry
            ?.savedStateHandle
            ?.set("location", jsonAdapter.toJson(Location(latitude, longitude)))
        returnBack()
    }


    override fun getNavController(): NavController {
        return navController
    }

    override fun returnBack() {
        navController.popBackStack()
    }

}
