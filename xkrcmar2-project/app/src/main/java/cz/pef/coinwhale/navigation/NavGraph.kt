package cz.pef.coinwhale.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import cz.pef.coinwhale.model.Location
import cz.pef.coinwhale.ui.screens.AccountListScreen
import cz.pef.coinwhale.ui.screens.AddEditAccountScreen
import cz.pef.coinwhale.ui.screens.AddEditCategoryScreen
import cz.pef.coinwhale.ui.screens.AddTransactionScreen
import cz.pef.coinwhale.ui.screens.CategoryListScreen
import cz.pef.coinwhale.ui.screens.MainPageScreen
import cz.pef.coinwhale.ui.screens.MapScreen
import cz.pef.coinwhale.ui.screens.StatisticsScreen
import cz.pef.coinwhale.ui.screens.TransactionListScreen

@Composable
fun NavGraph(
    navHostController: NavHostController = rememberNavController(),
    navigationRouter: INavigationRouter = remember {
        NavigationRouterImpl(navController = navHostController)
    },
    startDestination: String
) {
    NavHost(navController = navHostController, startDestination = startDestination) {

        composable(Destination.MainPageScreen.route){
            MainPageScreen(navigationRouter = navigationRouter)
        }

        composable(Destination.AccountListScreen.route){
            AccountListScreen(navigationRouter = navigationRouter)
        }

        composable(Destination.CategoryListScreen.route){
            CategoryListScreen(navigationRouter = navigationRouter)
        }


        composable(Destination.AddEditAccountScreen.route){
            AddEditAccountScreen(navigationRouter = navigationRouter)
        }


        composable(Destination.AddEditCategoryScreen.route){
            AddEditCategoryScreen(navigationRouter = navigationRouter,id = null)
        }

        composable(Destination.AddEditCategoryScreen.route + "/{id}", arguments = listOf(
            navArgument("id"){
                type = NavType.LongType
                defaultValue = -1L //nedokaze zpracovat null bohuzel
            }
        )){
            val id = it.arguments?.getLong("id")
            AddEditCategoryScreen(navigationRouter = navigationRouter, id = id)
        }

        composable(Destination.TransactionListScreen.route + "/{id}", arguments = listOf(
            navArgument("id"){
                type = NavType.LongType
                defaultValue = -1L
            }
        )){
            val id = it.arguments?.getLong("id")
            TransactionListScreen(navigationRouter = navigationRouter, id = id!!) //must have id because that it would not be possible to click on the account
        }

        composable(
            Destination.AddTransactionScreen.route + "/{id}/{transactionId}",
            arguments = listOf(
                navArgument("id") {
                    type = NavType.LongType
                },
                navArgument("transactionId") {
                    type = NavType.LongType
                    defaultValue = -1L // Set default value as needed
                }
            )
        ) {
            val id = it.arguments?.getLong("id") ?: -1L
            val transactionId = it.arguments?.getLong("transactionId")
            AddTransactionScreen(navigationRouter = navigationRouter, id = id, transactionId = transactionId)
        }

        composable(
            Destination.AddTransactionScreen.route + "/{id}",
            arguments = listOf(
                navArgument("id") {
                    type = NavType.LongType
                    defaultValue = -1L
                }
            )
        ) {
            val id = it.arguments?.getLong("id") ?: -1L
            AddTransactionScreen(navigationRouter = navigationRouter, id = id, transactionId = null)
        }



        composable(Destination.StatisticsScreen.route + "/{id}", arguments = listOf(
            navArgument("id"){
                type = NavType.LongType
                defaultValue = -1L
            }
        )){
            val id = it.arguments?.getLong("id")
            StatisticsScreen(navigationRouter = navigationRouter, id = id!!)
        }



        composable(Destination.MapScreen.route + "/{location}",
            arguments = listOf(
                navArgument("location") {
                    type = NavType.StringType
                    defaultValue = ""
                })
        ) {
            val locationString = it.arguments?.getString("location")
            if (!locationString.isNullOrEmpty()) {
                val moshi: Moshi = Moshi.Builder().build()
                val jsonAdapter: JsonAdapter<Location> = moshi.adapter(Location::class.java)
                val location = jsonAdapter.fromJson(locationString)
                MapScreen(navigationRouter = navigationRouter, latitude = location!!.latitude, longitute = location.longitude!!.toDouble())
            } else {
                // do Composable funkce už null poslat můžu.
                MapScreen(navigationRouter = navigationRouter, latitude = null, longitute = null)
            }
        }


        composable(Destination.MapScreen.route) {
            MapScreen(navigationRouter = navigationRouter, latitude = null, longitute = null)
        }



    }






}
