package cz.pef.coinwhale.ui.screens

sealed class AddEditCategoryUIState {

    object Loading : AddEditCategoryUIState()
    object CategorySaved : AddEditCategoryUIState()

    class CategoryStateChange(val data: AddEditCategoryData) : AddEditCategoryUIState()

    object CategoryDeleted : AddEditCategoryUIState()


}
