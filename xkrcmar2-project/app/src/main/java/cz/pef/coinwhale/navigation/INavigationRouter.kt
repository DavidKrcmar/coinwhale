package cz.pef.coinwhale.navigation

import androidx.navigation.NavController


interface INavigationRouter {

    fun navigateToCategoryList()
    fun navigateToAccountList()
    fun navigateToTransactionList(id: Long)
    fun navigateToAddTransaction(id: Long,transactionId: Long?)
    fun navigateToAddEditAccount(id: Long?)
    fun navigateToAddEditCategory(id: Long?)
    fun navigateToStatistics(id: Long)
    fun getNavController(): NavController
    fun returnBack()

//Maps
    fun navigateToMap(latitude: Double?,longitude: Double?)
    fun returnFromMap(latitude: Double,longitude: Double)


}
