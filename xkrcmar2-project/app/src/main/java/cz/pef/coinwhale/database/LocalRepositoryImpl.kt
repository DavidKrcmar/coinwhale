package cz.pef.coinwhale.database

import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction
import cz.pef.coinwhale.model.TransactionWithCategory
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalRepositoryImpl @Inject constructor(private val dao: Dao) : ILocalRepository {
    override fun getAllAccounts(): Flow<List<Account>> {
        return dao.getAllAccounts()
    }

    override suspend fun insertAccount(account: Account): Long {
        return dao.insertAccount(account)
    }

    override suspend fun updateAccount(account: Account) {
        return dao.updateAccount(account)
    }

    override suspend fun deleteAccount(account: Account) {
        return dao.deleteAccount(account)
    }

    override fun getAllCategories(): Flow<List<Category>> {
        return dao.getAllCategories()
    }

    override suspend fun insertCategory(category: Category): Long {
        return dao.insertCategory(category)
    }

    override suspend fun updateCategory(category: Category) {
        return dao.updateCategory(category)
    }

    override suspend fun deleteCategory(category: Category) {
        return dao.deleteCategory(category)
    }

    override suspend fun getCategory(id: Long): Category {
        return dao.getCategory(id)
    }

    override fun getOneTypeOfCategories(isIncome: Boolean): List<Category> {
        return dao.getOneTypeOfCategories(isIncome)
    }


    override suspend fun getAccountTransactions(accountId: Long): Flow<List<TransactionWithCategory>> {
        return dao.getAccountTransactions(accountId)
    }

    override suspend fun insertTransaction(transaction: Transaction): Long {
        return dao.insertTransaction(transaction)
    }

    override suspend fun updateTransaction(transaction: Transaction) {
        return dao.updateTransaction(transaction)
    }

    override suspend fun getTransactionDetail(transactionId: Long): Transaction {
        return dao.getTransactionDetail(transactionId)
    }

    override suspend fun deleteTransaction(transaction: Transaction) {
        return dao.deleteTransaction(transaction)
    }

}
