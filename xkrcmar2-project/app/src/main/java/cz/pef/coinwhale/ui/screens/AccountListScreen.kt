package cz.pef.coinwhale.ui.screens


import android.app.Dialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.pef.coinwhale.R
import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.navigation.INavigationRouter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AccountListScreen(navigationRouter: INavigationRouter) {

    val viewModel = hiltViewModel<AccountListViewModel>()

    val accounts: MutableList<Account> = mutableListOf()

    viewModel.accountListUIState.value.let {
        when(it){
            is AccountListUIState.Loading -> {
                viewModel.loadAccounts()
            }
            is AccountListUIState.Success -> {
                accounts.addAll(it.accounts)
            }
        }
    }




    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                title = {
                    Text(text = stringResource(id = R.string.accounts))
                },
            )},
        floatingActionButton = {
            FloatingActionButton(
                onClick = { navigationRouter.navigateToAddEditAccount(null) },
                content = {
                    Row(
                        modifier = Modifier.padding(8.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Icon(imageVector = Icons.Filled.Add, contentDescription = null)
                        Text(text = stringResource(id = R.string.add_account))
                    }
                }
            )
        }

        ) {
        AccountListScreenContent(paddingValues = it, accounts = accounts, navigationRouter = navigationRouter)
    }
}

@Composable
fun AccountListScreenContent(
    paddingValues: PaddingValues,
    accounts: List<Account>,
    navigationRouter: INavigationRouter
) {

    LazyColumn(
        modifier = Modifier.padding(paddingValues)
    ) {
        accounts.forEach {
            item {
                AccountListRow(account = it,
                    onClick = {
                        navigationRouter.navigateToTransactionList(it.id!!)
                    })
            }
        }

    }


}


@Composable
fun AccountListRow(
    account: Account,
    onClick: () -> Unit){


    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onClick()
            }
            .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(imageVector = Icons.Default.AccountCircle, contentDescription = null, modifier = Modifier.size(32.dp))
        Spacer(modifier = Modifier.width(16.dp))
        Text(text = account.name)
    }

}

