package cz.pef.coinwhale.ui.screens

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.pef.coinwhale.database.ILocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountListViewModel @Inject constructor(private val repository: ILocalRepository) : ViewModel(),AccountListScreenActions {

    val accountListUIState: MutableState<AccountListUIState> = mutableStateOf(AccountListUIState.Loading())

    fun loadAccounts(){
        viewModelScope.launch {
            repository.getAllAccounts().collect {
                accountListUIState.value = AccountListUIState.Success(it)
            }
        }
    }


}
