package cz.pef.coinwhale.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.map

class StoreCurrency(private val context: Context) {

    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("UserCurrency")
        val CURRENCY_USER_KEY = stringPreferencesKey("user_currency")
    }

    val getCurrency = context.dataStore.data.map {
        preferences ->
        preferences[CURRENCY_USER_KEY]
    }

    suspend fun clearCurrencyData() {
        context.dataStore.edit { preferences ->
            preferences.remove(CURRENCY_USER_KEY)
        }
    }



    suspend fun saveCurrency(name: String){
        context.dataStore.edit {
            preferences ->
            preferences[CURRENCY_USER_KEY] = name
        }
    }



}