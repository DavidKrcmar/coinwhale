package cz.pef.coinwhale.ui.elements

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import cz.pef.coinwhale.R
import cz.pef.coinwhale.ui.screens.AddEditCategoryData
import cz.pef.coinwhale.ui.screens.AddEditCategoryScreenActions

@Composable
fun TwoStateToggle(actions: AddEditCategoryScreenActions,data: AddEditCategoryData) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 32.dp, end = 32.dp),
            horizontalArrangement = Arrangement.Center
    ) {
        if(data.category.isIncome){
            Button(onClick = { actions.onTypeChange(true) }) {
                Text(text = stringResource(id = R.string.income))
            }
        }else{
            OutlinedButton(onClick = { actions.onTypeChange(true) },
            ) {
                Text(text = stringResource(id = R.string.income))
            }
        }
        Spacer(modifier = Modifier.width(32.dp))
        if(!data.category.isIncome){
            Button(onClick = { actions.onTypeChange(false) }) {
                Text(text = stringResource(id = R.string.expense))
            }
        }else{
            OutlinedButton(onClick = { actions.onTypeChange(false) }) {
                Text(text = stringResource(id = R.string.expense))
            }
        }



    }


}