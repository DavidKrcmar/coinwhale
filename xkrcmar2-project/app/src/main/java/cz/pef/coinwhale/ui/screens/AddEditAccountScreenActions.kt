package cz.pef.coinwhale.ui.screens

import androidx.compose.ui.graphics.Color


interface AddEditAccountScreenActions {
    fun saveAccount()
    fun onTextChange(text: String)

}
