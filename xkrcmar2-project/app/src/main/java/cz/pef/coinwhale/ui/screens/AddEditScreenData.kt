package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.Account

class AddEditScreenData {
    var account: Account = Account("")
    var taskNameError: String? = null
}
