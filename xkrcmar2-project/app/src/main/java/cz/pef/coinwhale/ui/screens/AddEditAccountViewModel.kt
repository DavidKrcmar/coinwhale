package cz.pef.coinwhale.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.pef.coinwhale.database.ILocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddEditAccountViewModel @Inject constructor(private val repository: ILocalRepository) : ViewModel(), AddEditAccountScreenActions {

    var data: AddEditScreenData = AddEditScreenData()

    private val _addEditAccountUIState: MutableStateFlow<AddEditAccountUIState> = MutableStateFlow(value = AddEditAccountUIState.Loading)
    val addEditAccountUIState = _addEditAccountUIState.asStateFlow()
    override fun saveAccount() {
        if (data.account.name.isNotEmpty()) {
            viewModelScope.launch {
                if (data.account.id == null) {
                    repository.insertAccount(data.account)
                }
                _addEditAccountUIState.update {
                    AddEditAccountUIState.AccountSaved
                }
            }

        }else{
            data.taskNameError = "Cannot be empty"
            _addEditAccountUIState.update {
                AddEditAccountUIState.AccountStateChange(data)
            }
        }

    }

    override fun onTextChange(text: String) {
        data.account.name = text
        _addEditAccountUIState.update {
            AddEditAccountUIState.AccountStateChange(data)
        }

    }


}
