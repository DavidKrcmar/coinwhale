package cz.pef.coinwhale.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction

@Database(entities = [Account::class, Category::class, Transaction::class], version = 18, exportSchema = true)
abstract class CoinWhaleDatabase: RoomDatabase() {
    abstract fun Dao(): Dao
    companion object {

        private var INSTANCE: CoinWhaleDatabase? = null

        fun getDatabase(context: Context): CoinWhaleDatabase {
            if (INSTANCE == null) {
                synchronized(CoinWhaleDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            CoinWhaleDatabase::class.java, "coin_whale_database"
                        ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
