package cz.pef.coinwhale.ui.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapEffect
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import cz.pef.coinwhale.navigation.INavigationRouter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MapScreen(
    navigationRouter: INavigationRouter,
    latitude: Double?,
    longitute: Double?
) {

    val viewModel = hiltViewModel<MapViewModel>()


    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                title = {
                    Text(text = "Mapa")
                },
                actions = {
                    IconButton(onClick = {
                        if(viewModel.locationChanged){
                            navigationRouter.returnFromMap(viewModel.latitude!!, viewModel.longitute!!)
                        }else{
                            navigationRouter.returnBack()
                        }
                    }) {
                        Icon(imageVector = Icons.Default.Done, contentDescription = null)
                    }
                })
        },

        ) {
        MapScreenContent(
            paddingValues = it,
            actions = viewModel,
            latitude = if(latitude != null) latitude else 49.0,
            longitute = if(longitute != null) longitute else 16.0
        )
    }
}

@Composable
fun MapScreenContent(paddingValues: PaddingValues,
                     latitude: Double,
                     longitute: Double,
                     actions: MapScreenActions
) {

    val cameraPositionState = rememberCameraPositionState{
        position = CameraPosition.fromLatLngZoom(LatLng(latitude,longitute),10.0f)
    }

    val markerPosition = rememberMarkerState(position = LatLng(latitude,longitute))

    Box(modifier = Modifier
        .fillMaxSize()
        .padding(paddingValues)){
        GoogleMap(cameraPositionState = cameraPositionState) {


            MapEffect{
                it.setOnMarkerDragListener(object : OnMarkerDragListener{
                    override fun onMarkerDrag(p0: Marker) {}

                    override fun onMarkerDragEnd(p0: Marker) {
                        actions.onLocationChange(p0.position.latitude,p0.position.longitude)
                    }

                    override fun onMarkerDragStart(p0: Marker) {}

                })
            }
            Marker(state = markerPosition, draggable = true)

        }
    }


}
