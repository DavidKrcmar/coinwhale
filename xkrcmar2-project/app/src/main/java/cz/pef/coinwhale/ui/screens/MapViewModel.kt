package cz.pef.coinwhale.ui.screens

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class MapViewModel @Inject constructor() : ViewModel(), MapScreenActions {

    var latitude: Double? = null
    var longitute: Double? = null
    var locationChanged: Boolean = false
    override fun onLocationChange(latitude: Double, longitude: Double) {
        this.latitude = latitude
        this.longitute = longitude
        locationChanged = true
    }


}
