package cz.pef.coinwhale.navigation

sealed class Destination(val route: String) {
    object AccountListScreen : Destination("account_list")
    object CategoryListScreen : Destination("category_list")
    object TransactionListScreen : Destination("transaction_list")
    object MainPageScreen : Destination("main_screen")
    object AddEditAccountScreen : Destination("add_edit_account")
    object AddEditCategoryScreen : Destination("add_edit_category")
    object AddTransactionScreen : Destination("add_transaction")
    object StatisticsScreen : Destination("statistics_screen")

    object MapScreen : Destination("map")

}
