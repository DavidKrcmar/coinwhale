package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction

class AddTransactionData {
    var transaction: Transaction = Transaction("")
    var categories: List<Category> = mutableListOf()
    var nameError: String? = null
    var categoryError: String? = null
}
