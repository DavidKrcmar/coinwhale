package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.Category


interface AddTransactionScreenActions {
    fun saveTransaction()
    fun onTextChange(text: String)
    fun onDescriptionChange(text: String)
    fun onPriceChange(price: Double)
    fun onCategoryChange(category: Category)

    fun deleteTransaction()
    fun onLocationChange(latitude: Double?, longitude: Double?)


}
