package cz.pef.coinwhale

import android.app.Application
import dagger.hilt.android.HiltAndroidApp




@HiltAndroidApp
class CoinWhaleApplication: Application() {

}
