package cz.pef.coinwhale.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category")
data class Category(var name: String){

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null

    var color: Int? = null

    var isIncome: Boolean = true

}
