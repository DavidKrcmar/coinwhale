package cz.pef.coinwhale.datastore

import android.content.Context

object StoreCurrencyProvider {
    private lateinit var instance: StoreCurrency

    fun initialize(context: Context) {
        instance = StoreCurrency(context)
    }

    fun getInstance(): StoreCurrency {
        if (!::instance.isInitialized) {
            throw IllegalStateException("StoreCurrencyProvider must be initialized")
        }
        return instance
    }
}