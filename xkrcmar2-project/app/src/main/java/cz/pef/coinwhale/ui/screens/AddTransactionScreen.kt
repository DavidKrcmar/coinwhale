package cz.pef.coinwhale.ui.screens


import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.compose.LifecycleEventEffect
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import cz.pef.coinwhale.R
import cz.pef.coinwhale.model.Location
import cz.pef.coinwhale.navigation.INavigationRouter
import cz.pef.coinwhale.ui.elements.CategorySelector
import cz.pef.coinwhale.ui.elements.ColorPicker
import cz.pef.coinwhale.ui.elements.InfoElement
import cz.pef.coinwhale.ui.elements.TwoStateToggle
import cz.pef.coinwhale.ui.extensions.getValue
import cz.pef.coinwhale.ui.extensions.removeValue
import cz.pef.coinwhale.ui.extensions.round


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddTransactionScreen(navigationRouter: INavigationRouter,id: Long,transactionId: Long?) {

    val viewModel = hiltViewModel<AddTransactionViewModel>()
    var data by remember {
        mutableStateOf(AddTransactionData())
    }


    LifecycleEventEffect(event = Lifecycle.Event.ON_RESUME) {
        val mapLocationResult = navigationRouter.getNavController().getValue<String>("location")
        mapLocationResult?.value?.let {
            val moshi: Moshi = Moshi.Builder().build()
            val jsonAdapter: JsonAdapter<Location> = moshi.adapter(Location::class.java)
            val location = jsonAdapter.fromJson(it)
            navigationRouter.getNavController().removeValue<String>("location") //musime ji odstranit po prevzeti
            location?.let {
                viewModel.onLocationChange(it.latitude,it.longitude)
            }
        }
    }

    val state = viewModel.addTransactionUIState.collectAsState()

    state.value.let {
        when(it){
            AddTransactionUIState.Loading -> {
                viewModel.loadTransaction(transactionId)
                viewModel.loadCategories()
            }
            AddTransactionUIState.TransactionSaved -> {
                LaunchedEffect(it){
                    navigationRouter.returnBack()
                }
            }
            is AddTransactionUIState.TransactionStateChange -> {
                data.transaction.accountId = id
                data = it.data
            }

            is AddTransactionUIState.TransactionDeleted -> {
                LaunchedEffect(it){
                    navigationRouter.returnBack()
                }
            }

        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                actions = {
                          if(transactionId != null){
                              IconButton(onClick = { viewModel.deleteTransaction() }) {
                                  Icon(imageVector = Icons.Default.Delete, contentDescription = null)
                              }
                          }
                },
                
                navigationIcon = {
                    
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },

                title = {
                    Text(text = stringResource(id = R.string.add_transaction))
                },


                )},


        ) {
        AddTransactionScreenContent(
            paddingValues = it,
            data = data,
            actions = viewModel,
            navigationRouter = navigationRouter
        )
    }
}


@Composable
fun AddTransactionScreenContent(
    paddingValues: PaddingValues,
    actions: AddTransactionScreenActions,
    data: AddTransactionData,
    navigationRouter: INavigationRouter
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .align(Alignment.Center)
        ) {

            OutlinedTextField(
                label = { Text(stringResource(id = R.string.name)) },
                value = data.transaction.transactionName,
                onValueChange = { actions.onTextChange(it) },
                isError = data.nameError != null,
                supportingText = {
                    if (data.nameError != null)
                        Text(text = data.nameError!!)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 16.dp, end = 16.dp)
            )

            OutlinedTextField(
                label = { Text(stringResource(id = R.string.price)) },
                isError = data.nameError != null,
                supportingText = {
                    if (data.nameError != null)
                        Text(text = data.nameError!!)
                },
                value = if (data.transaction.price == 0.0) {
                    ""
                } else {
                    data.transaction.price.toString()
                },
                onValueChange = { actions.onPriceChange(it.toDouble()) },
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 16.dp, end = 16.dp)
            )



            OutlinedTextField(
                label = { Text(stringResource(id = R.string.description)) },
                value = data.transaction.description ?: "",
                onValueChange = { actions.onDescriptionChange(it) },
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 16.dp, end = 16.dp)
            )

            Spacer(modifier = Modifier.padding(vertical = 8.dp))

            InfoElement(
                modifier = Modifier.fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .padding(start = 16.dp, end = 16.dp),
                leadingIcon = Icons.Filled.LocationOn,
                value = if (data.transaction.latitude != null && data.transaction.longitude != null){
                    "${data.transaction.latitude!!.round()}, ${data.transaction.longitude!!.round()}"
                }else null,
                hint = "Location",
                onClearClick = { actions.onLocationChange(null,null) },
                onClick = {
                    navigationRouter.navigateToMap(data.transaction.latitude,data.transaction.longitude)
                })

            CategorySelector(actions = actions, data = data)





        }



        Button(
            onClick = { actions.saveTransaction() },
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
                .height(56.dp)
        ) {
            Text(text = stringResource(id = R.string.save))
        }
    }
}
