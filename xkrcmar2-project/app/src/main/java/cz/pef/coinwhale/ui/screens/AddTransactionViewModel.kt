package cz.pef.coinwhale.ui.screens

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.pef.coinwhale.database.ILocalRepository
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddTransactionViewModel @Inject constructor(private val repository: ILocalRepository) : ViewModel(), AddTransactionScreenActions {

    var data: AddTransactionData = AddTransactionData()

    private val _addTransactionUIState: MutableStateFlow<AddTransactionUIState> = MutableStateFlow(value = AddTransactionUIState.Loading)
    val addTransactionUIState = _addTransactionUIState.asStateFlow()
    override fun saveTransaction() {
        if (data.transaction.transactionName.isNotEmpty() && data.transaction.categoryId != -1L && data.transaction.price != 0.0) {
            viewModelScope.launch {
                if (data.transaction.transactionId == null) {
                    repository.insertTransaction(data.transaction)
                }else{
                    repository.updateTransaction(data.transaction)
                }
                _addTransactionUIState.update {
                    AddTransactionUIState.TransactionSaved
                }
            }

        }else{
            data.nameError = "Cannot be empty"
            _addTransactionUIState.update {
                AddTransactionUIState.TransactionStateChange(data)
            }
        }
    }

    override fun onTextChange(text: String) {
        data.transaction.transactionName = text
        _addTransactionUIState.update {
            AddTransactionUIState.TransactionStateChange(data)
        }
    }

    override fun onDescriptionChange(text: String) {
        data.transaction.description = text
        _addTransactionUIState.update {
            AddTransactionUIState.TransactionStateChange(data)
        }
    }

    override fun onPriceChange(price: Double) {
        data.transaction.price = price
        _addTransactionUIState.update {
            AddTransactionUIState.TransactionStateChange(data)
        }
    }



    override fun onCategoryChange(category: Category) {
        if (category.id != null){
            data.transaction.categoryId = category.id!!
            _addTransactionUIState.update {
                AddTransactionUIState.TransactionStateChange(data)
            }
        }else{
            data.categoryError = "select category if there is not any you must created it in categories"
            _addTransactionUIState.update {
                AddTransactionUIState.TransactionStateChange(data)
        }
    }}

    override fun deleteTransaction() {
        viewModelScope.launch {
            repository.deleteTransaction(data.transaction)
            _addTransactionUIState.update {
                AddTransactionUIState.TransactionDeleted
            }
        }

    }


    fun loadCategories(){
        viewModelScope.launch {
            repository.getAllCategories().collect { categories ->
                data.categories = categories
                _addTransactionUIState.value = AddTransactionUIState.TransactionStateChange(data)
            }
        }
    }


    fun loadTransaction(transactionId: Long?){
        if(transactionId != null){
            viewModelScope.launch {
                data.transaction = repository.getTransactionDetail(transactionId)
            }
            _addTransactionUIState.update {
                AddTransactionUIState.TransactionStateChange(data)
            }
        }
    }

    override fun onLocationChange(latitude: Double?, longitude: Double?) {
        data.transaction.latitude = latitude
        data.transaction.longitude = longitude
        _addTransactionUIState.update {
            AddTransactionUIState.TransactionStateChange(data)
        }
    }



}
