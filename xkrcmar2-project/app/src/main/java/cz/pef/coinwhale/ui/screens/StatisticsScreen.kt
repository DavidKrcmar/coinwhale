package cz.pef.coinwhale.ui.screens



import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import app.futured.donut.compose.DonutProgress
import app.futured.donut.compose.data.DonutModel
import app.futured.donut.compose.data.DonutSection
import cz.pef.coinwhale.datastore.StoreCurrencyProvider
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.TransactionWithCategory
import cz.pef.coinwhale.navigation.INavigationRouter


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StatisticsScreen(navigationRouter: INavigationRouter,id: Long) {

    val viewModel = hiltViewModel<StatisticsViewModel>()

    val data: MutableList<TransactionWithCategory> = mutableListOf()


    viewModel.statisticsListUIState.value.let {
        when(it){
            is StatisticsListUIState.Loading -> {
                viewModel.loadCategories(id)
            }
            is StatisticsListUIState.Success -> {
                data.addAll(it.categories)
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                },
                title = {
                    Text(text = "Statistics")
                },
            )},


    ) {
        StatisticsScreenContent(paddingValues = it, navigationRouter = navigationRouter, data = data)
    }
}

@Composable
fun StatisticsScreenContent(
    paddingValues: PaddingValues,
    navigationRouter: INavigationRouter,
    data: List<TransactionWithCategory>,
) {
    val savedCurrency = StoreCurrencyProvider.getInstance().getCurrency.collectAsState(initial = "")
    val chart by remember(data) {
        mutableStateOf(data.groupBy { it.category }.values.flatten().take(1))
    }

    val groupedTransactions by remember(data) {
        mutableStateOf(data.groupBy { it.category })
    }

    Column {
        LazyColumn(modifier = Modifier.padding(paddingValues)){
            chart.forEach { _ ->
                item {
                    SimpleDonutChart(data = groupedTransactions, textData = data, sign = savedCurrency.value!!)
                }}
        }


        LazyColumn(modifier = Modifier.padding(paddingValues)) {
            groupedTransactions.forEach { (category, transactions) ->
                item {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {

                        Text(
                            modifier = Modifier
                                .padding(16.dp)
                                .drawBehind {
                                    drawCircle(
                                        color = Color(category.color!!),
                                        radius = this.size.maxDimension
                                    )
                                },
                            text = category.name[0].toString().uppercase(),
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Bold
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        Column {
                            Text(category.name)
                            if (category.isIncome) {
                                Text(text = "Income", fontSize = 12.sp, color = Color.Gray)
                            } else {
                                Text(text = "Expense", fontSize = 12.sp, color = Color.Gray)
                            }
                        }
                        Spacer(modifier = Modifier.weight(1f))
                        Text(
                            text = transactions.sumByDouble { it.transaction.price }
                                .toString() + savedCurrency.value, fontSize = 20.sp, modifier = Modifier
                                .padding(end = 8.dp)
                        )


                    }
                }

            }
        }
    }

}


@Composable
fun SimpleDonutChart(data: Map<Category, List<TransactionWithCategory>>, textData: List<TransactionWithCategory>,sign: String) {

    val totalTransactionValue = data.values.flatten().sumByDouble { it.transaction.price }

    val sections = data.map { (category, transactions) ->
        val categoryTransactionValue = transactions.sumByDouble { it.transaction.price }
        val proportion = categoryTransactionValue / totalTransactionValue
        DonutSection(amount = proportion.toFloat(), color = Color(category.color!!))
    }

    val donutModel = DonutModel(
        cap = 1f,
        masterProgress = 1f,
        gapWidthDegrees = 0f,
        sections = sections,
        strokeWidth = 50f
    )

    Box(modifier = Modifier
        .fillMaxSize()
        .padding(16.dp), contentAlignment = Alignment.Center) {
        DonutProgress(
            model = donutModel,
            modifier = Modifier.size(200.dp)
        )
        Text(
            text = getSum(data = textData).toString() + sign,
            style = TextStyle(fontSize = 25.sp, color = Color.Black)
        )
    }
}


