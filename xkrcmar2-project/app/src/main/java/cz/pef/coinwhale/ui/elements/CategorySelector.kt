package cz.pef.coinwhale.ui.elements

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import cz.pef.coinwhale.ui.screens.AddTransactionData
import cz.pef.coinwhale.ui.screens.AddTransactionScreenActions
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import cz.pef.coinwhale.R

@Composable
fun CategorySelector(actions: AddTransactionScreenActions,data: AddTransactionData) {
    var isIncome by remember { mutableStateOf(true) }
    var expanded by remember { mutableStateOf(false) }
    var selectedCategory by remember {
        mutableStateOf("")
    }


    Column(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 16.dp, end = 16.dp),) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 32.dp, end = 32.dp, top = 32.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        if(isIncome){
            Button(onClick = {selectedCategory = "";isIncome = true }) {
                Text(text = stringResource(id = R.string.income))
            }
        }else{
            OutlinedButton(onClick = {selectedCategory = "";isIncome = true },
            ) {
                Text(text = stringResource(id = R.string.income))
            }
        }
        Spacer(modifier = Modifier.width(32.dp))
        if(!isIncome){
            Button(onClick = {selectedCategory = "";isIncome = false }) {
                Text(text = stringResource(id = R.string.expense))
            }
        }else{
            OutlinedButton(onClick = {selectedCategory = "";isIncome = false }) {
                Text(text = stringResource(id = R.string.expense))
            }
        }

    }
        Text(text = stringResource(id = R.string.categories))
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = { expanded = !expanded })
                .border(width = 2.dp, Color.Gray)
                .padding(8.dp),
            contentAlignment = Alignment.CenterStart
        ) {

            Text(text = selectedCategory.takeIf { it.isNotEmpty() } ?: stringResource(id = R.string.select_category))

            Icon(
                modifier = Modifier
                    .size(24.dp)
                    .align(Alignment.CenterEnd),
                imageVector = if (expanded) Icons.Default.KeyboardArrowUp else Icons.Default.KeyboardArrowDown,
                contentDescription = null,
                tint = Color.Black
            )

            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },

            ) {
                data.categories.forEach { category ->
                    if(category.isIncome == isIncome){
                        DropdownMenuItem(
                            text = { Text(text = category.name) },
                            onClick = {selectedCategory = category.name;actions.onCategoryChange(category); expanded = !expanded })
                    }

                }
            }
        }

        if (data.nameError != null) {
            Text(
                text = data.nameError!!,
                color = Color.Red,
                modifier = Modifier
                    .padding(top = 10.dp)
                    .padding(horizontal = 16.dp)
            )
        }


    }


}


