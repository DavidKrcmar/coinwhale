package cz.pef.coinwhale.ui.screens

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.pef.coinwhale.database.ILocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddEditCategoryViewModel @Inject constructor(private val repository: ILocalRepository) : ViewModel(), AddEditCategoryScreenActions {

    var data: AddEditCategoryData = AddEditCategoryData()

    private val _addEditCategoryUIState: MutableStateFlow<AddEditCategoryUIState> = MutableStateFlow(value = AddEditCategoryUIState.Loading)
    val addEditCategoryUIState = _addEditCategoryUIState.asStateFlow()
    override fun saveCategory() {
        if (data.category.name.isNotEmpty()) {
            viewModelScope.launch {
                if (data.category.id == null) {
                    repository.insertCategory(data.category)
                }else{
                    repository.updateCategory(data.category)
                }
                _addEditCategoryUIState.update {
                    AddEditCategoryUIState.CategorySaved
                }
            }

        }else{
            data.NameError = "Cannot be empty"
            _addEditCategoryUIState.update {
                AddEditCategoryUIState.CategoryStateChange(data)
            }
        }
    }

    override fun onTextChange(text: String) {
        data.category.name = text
        _addEditCategoryUIState.update {
            AddEditCategoryUIState.CategoryStateChange(data)
        }
    }

    override fun onColorChange(color: Color) {
        data.category.color = color.toArgb()
        _addEditCategoryUIState.update {
            AddEditCategoryUIState.CategoryStateChange(data)
        }
    }

    override fun onTypeChange(type: Boolean) {
        data.category.isIncome = type
        _addEditCategoryUIState.update {
            AddEditCategoryUIState.CategoryStateChange(data)
        }
    }

    override fun deleteCategory() {
        viewModelScope.launch {
            repository.deleteCategory(data.category)
            _addEditCategoryUIState.update {
                AddEditCategoryUIState.CategoryDeleted
            }
        }

    }

    fun loadCategory(id: Long?){
        if(id != null){
            viewModelScope.launch {
                data.category = repository.getCategory(id)
            }
            _addEditCategoryUIState.update {
                AddEditCategoryUIState.CategoryStateChange(data)
            }
        }
    }



}
