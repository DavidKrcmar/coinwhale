package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.TransactionWithCategory

sealed class StatisticsListUIState {
    class Loading : StatisticsListUIState()
    class Success(val categories: List<TransactionWithCategory>) : StatisticsListUIState()




}