package cz.pef.coinwhale.ui.screens

import androidx.compose.ui.graphics.Color

interface AddEditCategoryScreenActions {
    fun saveCategory()
    fun onTextChange(text: String)

    fun onColorChange(color: Color)

    fun onTypeChange(type: Boolean)

    fun deleteCategory()

}
