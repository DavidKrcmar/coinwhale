package cz.pef.coinwhale.ui.extensions

fun Double.round(): String{
    return String.format("%.2f", this)
}
