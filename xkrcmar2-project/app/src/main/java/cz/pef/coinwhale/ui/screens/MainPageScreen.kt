package cz.pef.coinwhale.ui.screens

import android.annotation.SuppressLint
import android.content.ClipData.Item
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.NavController
import cz.pef.coinwhale.R
import cz.pef.coinwhale.datastore.StoreCurrency
import cz.pef.coinwhale.datastore.StoreCurrencyProvider
import cz.pef.coinwhale.navigation.INavigationRouter
import kotlinx.coroutines.launch
import java.util.Currency



@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainPageScreen(navigationRouter: INavigationRouter) {

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_name))
                },
            )},

        ) {
        MainPageScreenContent(paddingValues = it, navRouter = navigationRouter)
    }
}



@Composable
fun MainPageScreenContent(
    paddingValues: PaddingValues,
    navRouter: INavigationRouter,
) {
    val context = LocalContext.current
    val versionName = context.packageManager.getPackageInfo(context.packageName, 0).versionName
    val savedCurrency = StoreCurrencyProvider.getInstance().getCurrency.collectAsState(initial = "")
    var showDialog by remember { mutableStateOf(savedCurrency.value == null) }

    SimpleDialogWithTextField(
        showDialog = savedCurrency.value == null,
        onCloseDialog = { showDialog = false },
    )

    Column(modifier = Modifier.fillMaxSize()) {
        LazyColumn(
            modifier = Modifier.padding(paddingValues)
        ) {
            item {
                TaskListRow(
                    text = stringResource(id = R.string.accounts),
                    onClick = { navRouter.navigateToAccountList() })
            }
            item {
                TaskListRow(
                    text = stringResource(id = R.string.categories),
                    onClick = { navRouter.navigateToCategoryList() })
            }


        }
        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = "Version: $versionName", color = Color.Gray,textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(32.dp)
        )
    }

}


@Composable
fun TaskListRow(
    text: String,
    onClick: () -> Unit){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onClick()
            }
            .padding(16.dp)
    ) {
        Text(text = text)
        }


}


@Composable
fun SimpleDialogWithTextField(
    showDialog: Boolean,
    onCloseDialog: () -> Unit,
) {
    val scope = rememberCoroutineScope()
    var currency by remember { mutableStateOf("") }

    if (showDialog) {
        AlertDialog(
            onDismissRequest = {  },
            title = { Text(text = stringResource(id = R.string.currency_text)) },
            text = {

                TextField(
                    value = currency,
                    onValueChange = { currency = it },
                    label = { Text(text = stringResource(id = R.string.currency)) }
                )
            },
            confirmButton = {
                Button(
                    onClick = {
                        if (currency.isNotBlank()) {
                            scope.launch {
                                StoreCurrencyProvider.getInstance().saveCurrency(currency)
                            }
                            onCloseDialog()
                        }

                    }
                ) {
                    Text(text = stringResource(id = R.string.save))
                }
            },

            )
    }
}