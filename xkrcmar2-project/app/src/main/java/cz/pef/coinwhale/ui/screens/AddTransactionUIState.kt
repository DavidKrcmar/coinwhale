package cz.pef.coinwhale.ui.screens

sealed class AddTransactionUIState {

    object Loading : AddTransactionUIState()
    object TransactionSaved : AddTransactionUIState()
    class TransactionStateChange(val data: AddTransactionData) : AddTransactionUIState()

    object TransactionDeleted : AddTransactionUIState()
}
