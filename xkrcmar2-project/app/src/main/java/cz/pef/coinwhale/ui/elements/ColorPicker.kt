package cz.pef.coinwhale.ui.elements

import android.graphics.PointF
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Gray
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.unit.dp
import com.github.skydoves.colorpicker.compose.AlphaTile
import com.github.skydoves.colorpicker.compose.HsvColorPicker
import com.github.skydoves.colorpicker.compose.rememberColorPickerController
import cz.pef.coinwhale.ui.screens.AddEditCategoryData
import cz.pef.coinwhale.ui.screens.AddEditCategoryScreenActions


@Composable
fun ColorPicker(actions: AddEditCategoryScreenActions,data: AddEditCategoryData) {
    val controller = rememberColorPickerController()
    var isPickerVisible by remember { mutableStateOf(false) }

    if (data.category.color == null){
        actions.onColorChange(Color.Magenta)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 16.dp, end = 16.dp, top = 8.dp)
    ) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        AlphaTile(
            modifier = Modifier
                .fillMaxWidth()
                .height(40.dp)
                .clip(RoundedCornerShape(8.dp))
                .clickable(onClick = { isPickerVisible = !isPickerVisible })
                .border(width = 1.dp, color = Gray),
//            controller = controller,
            selectedColor = Color(data.category.color ?: Color.Magenta.toArgb())
        )
    }

        if(isPickerVisible) {
            HsvColorPicker(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .padding(16.dp),
                controller = controller,
                onColorChanged = {
                    actions.onColorChange(it.color)
                }
            )
        }






    }


}