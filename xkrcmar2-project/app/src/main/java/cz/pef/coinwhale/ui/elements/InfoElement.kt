package cz.pef.coinwhale.ui.elements

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp

@Composable
fun InfoElement(
    modifier: Modifier = Modifier,
    leadingIcon: ImageVector,
    value: String?,
    hint: String,
    onClearClick: () -> Unit,
    onClick: () -> Unit
){
    Row(
        modifier = modifier
            .fillMaxWidth()
            .clickable {
                onClick()
            }
            .padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(imageVector = leadingIcon, contentDescription = null)
        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Text(text = if (value != null) value else "Not set")
            Text(text = hint, color = Color.Gray)
        }
        Spacer(modifier = Modifier.weight(1.0f))
        if (value != null){
            IconButton(
                modifier = Modifier.size(24.dp),
                onClick = { onClearClick() }) {
                Icon(imageVector = Icons.Filled.Clear, contentDescription = null)
            }
        }

    }




}
