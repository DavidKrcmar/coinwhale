package cz.pef.coinwhale.ui.screens

import cz.pef.coinwhale.model.Account


sealed class AccountListUIState {
    class Loading : AccountListUIState()
    class Success(val accounts: List<Account>) : AccountListUIState()




}
