package cz.pef.coinwhale.database

import cz.pef.coinwhale.model.Account
import cz.pef.coinwhale.model.Category
import cz.pef.coinwhale.model.Transaction
import cz.pef.coinwhale.model.TransactionWithCategory
import kotlinx.coroutines.flow.Flow

interface ILocalRepository {
    fun getAllAccounts(): Flow<List<Account>>
    suspend fun insertAccount(account: Account): Long

    suspend fun updateAccount(account: Account)

    suspend fun deleteAccount(account: Account)


    fun getAllCategories(): Flow<List<Category>>
    suspend fun insertCategory(category: Category): Long

    suspend fun updateCategory(category: Category)

    suspend fun deleteCategory(category: Category)

    suspend fun getCategory(id: Long): Category

    fun getOneTypeOfCategories(isIncome: Boolean): List<Category>


    suspend fun getAccountTransactions(accountId: Long): Flow<List<TransactionWithCategory>>

    suspend fun insertTransaction(transaction: Transaction): Long

    suspend fun updateTransaction(transaction: Transaction)

    suspend fun  getTransactionDetail(transactionId: Long): Transaction

    suspend fun deleteTransaction(transaction: Transaction)

}
